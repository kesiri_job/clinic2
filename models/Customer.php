<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $customer_id
 * @property string $customer_name
 * @property string $phone
 * @property string $picture
 * @property int $age
 * @property double $height
 * @property double $weight
 * @property string $nicname
 * @property string $gender
 * @property string $career
 * @property string $disease
 * @property string $id_card
 * @property string $history_allergy
 * @property string $birth_dath
 */
class Customer extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'picture',
            ]
        ];
    }
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['age'], 'integer','min' => 1],
            [['height', 'weight'], 'number','min' => 1],
            [['file','birth_dath'], 'safe'],
            [['customer_name'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 10],
            [[ 'disease','picture', 'history_allergy'], 'string', 'max' => 255],
            [['nicname', 'gender'], 'string', 'max' => 20],
            [['career'], 'string', 'max' => 100],
            [['id_card'], 'string', 'max' => 13],
            [['customer_name','birth_dath'], 'required'],
            [['id_card'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'customer_name' => 'ชื่อ',
            'phone' => 'เบอร์โทร',
            'picture' => 'Picture',
            'age' => 'อายุ',
            'height' => 'ส่วนสูง',
            'weight' => 'น้ำหนัก',
            'nicname' => 'ชื่อเล่น',
            'gender' => 'เพศ',
            'career' => 'อาชีพ',
            'disease' => 'โรคประจำตัว',
            'id_card' => 'รหัสบัตรประชาชน',
            'history_allergy' => 'ประวัติการแพ้ยา',
            'birth_dath' => 'วันเกิด',
        ];
    }
    public function getPicture()
    {
        return $this->picture? Yii::getAlias('@web/uploads/').$this->picture:Yii::getAlias('@web/uploads/nopic.png');
    }
    public function getMeetings()
    {
        return $this->hasMany(Meeting::className(), ['customer_id' => 'customer_id']);
    }

    public function getMeeting()
    {
        return $this->hasOne(Meeting::className(), ['customer_id' => 'customer_id']);
    }
}
