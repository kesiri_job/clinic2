<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fat_list".
 *
 * @property int $id
 * @property string $fat_id_name
 * @property double $fat_cc
 * @property int $meet_id
 *
 * @property Meeting $meet
 */
class FatList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fat_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fat_cc'], 'number'],
            [['meet_id'], 'integer'],
            [['fat_id_name'], 'string', 'max' => 45],
            [['meet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meeting::className(), 'targetAttribute' => ['meet_id' => 'meet_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fat_id_name' => 'Fat Id Name',
            'fat_cc' => 'Fat Cc',
            'meet_id' => 'Meet ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeet()
    {
        return $this->hasOne(Meeting::className(), ['meet_id' => 'meet_id']);
    }

    public function getFatName(){
        if($this->fat_cc!=null){  return $this->fat_id_name."  ".$this->fat_cc." cc.<br/> ";
        }else{
            return null;
        }

    }
}
