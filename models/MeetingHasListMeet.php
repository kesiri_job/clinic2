<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meeting_has_list_meet".
 *
 * @property int $meet_id
 * @property int $list_id
 *
 * @property ListMeet $list
 * @property Meeting $meet
 */
class MeetingHasListMeet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meeting_has_list_meet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meet_id', 'list_id'], 'required'],
            [['meet_id', 'list_id'], 'integer'],
            [['meet_id', 'list_id'], 'unique', 'targetAttribute' => ['meet_id', 'list_id']],
            [['list_id'], 'exist', 'skipOnError' => true, 'targetClass' => ListMeet::className(), 'targetAttribute' => ['list_id' => 'list_id']],
            [['meet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meeting::className(), 'targetAttribute' => ['meet_id' => 'meet_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'meet_id' => 'Meet ID',
            'list_id' => 'List ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(ListMeet::className(), ['list_id' => 'list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeet()
    {
        return $this->hasOne(Meeting::className(), ['meet_id' => 'meet_id']);
    }
}
