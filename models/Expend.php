<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "expend".
 *
 * @property int $id
 * @property string $expend_name
 * @property int $expend_number
 * @property string $expend_date
 * @property string $detail
 * @property int $created_by
 * @property int $updated_by
 */
class Expend extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
        ];
    }
    public static function tableName()
    {
        return 'expend';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['expend_name', 'expend_number', 'expend_date'], 'required'],
            [['expend_number', 'created_by', 'updated_by'], 'integer'],
            [['expend_date'], 'safe'],
            [['expend_name', 'detail'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'expend_name' => 'รายจ่าย',
            'expend_number' => 'จำนวนเงิน',
            'expend_date' => 'วันที่',
            'detail' => 'รายละเอียด',
            'created_by' => 'ผู้เพิ่มข้อมูล',
            'updated_by'=>'ผู้แก้ไขข้อมูล',
        ];
    }
}
