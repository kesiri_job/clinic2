<?php

use kartik\grid\GridView;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel app\models\MeetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Meet';
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['customer/index']];
$this->params['breadcrumbs'][] = $customer->customer_name;

?>

<div class="col-lg-12">
    <div class="portlet">
        <div class="portlet-heading ">
            <h2 class="portlet-title text-dark">
                <?= Html::encode($this->title) ?>
            </h2>

            <div class="portlet-widgets">
                    <?= Html::a('<i class="fas fa-images"></i> Photo', ['photo?id=' . $customer->customer_id], ['class' => 'btn btn-default']) ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-success" data-toggle="dropdown" aria-expanded="false">
                        <i class="zmdi zmdi-collection-plus"></i> Create Meeting <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><?= Html::a('การรักษา เคสปกติ', ['create','id'=> $customer->customer_id,'active'=>'yes']) ?></li>
                        <li><?= Html::a('การรักษา เคสแก้ไข', ['create','id'=> $customer->customer_id,'status'=>'edit','active'=>'yes']) ?></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="bg-primary" class="panel-collapse collapse in">
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-9">
                        <?php echo $this->render('_search', ['model' => $searchModel, 'customer_id' => $customer_id]); ?>
                    </div>
                    <div class="col-md-3" align="right">
                        <blockquote class="blockquote-reverse"><p>จำนวนเงินรวม</p>
                            <footer><?= $sum ?><cite title="Source Title"> บาท</cite></footer>
                        </blockquote>
                    </div>
                </div>
                <br/>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'layout' => '{items}{summary}{pager}',
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'filter' => false,
                            'attribute' => 'meet_title',
                        ],
                        [
                            'header' => 'วันที่เข้าพบ',
                            'value' => function ($data) {
                                return \app\controllers\GetpublicController::getDateThai($data->meet_date);
                            }
                        ],
                        [
                            'label' => 'สถานะ',
                            'attribute' => 'status',
                            'format' => 'html',
                            'filter' => $searchModel->itemStatus,
                            'value' => function ($model, $key, $index, $column) {
                                return $model->status == 1 ? "<span class='label label-primary'>เคสปกติ</span>" : "<span class='label label-danger'>เคสแก้ไข</span>";
                            },
                            'filterType' => GridView::FILTER_SELECT2,
                            'filterWidgetOptions' => [
                                'pluginOptions' => ['allowClear' => true],
                            ],
                            'filterInputOptions' => ['placeholder' => 'สถานะ'],
                        ],
                        ['class' => 'yii\grid\ActionColumn',
                            'buttonOptions' => ['class' => 'btn btn-default'],
                            'options' => ['style' => 'width:130px;'],
                            'template' => '<div class="btn-group btn-group-sm text-center" role="group"> {view} {update} {delete} </div>',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return $model->status==0?
                                        Html::a('<i class="far fa-eye"></i>', ['view', 'id' => $model->meet_id, 'customer' => $model->customer_id,'status'=>'edit'], [
                                            'class' => 'btn btn-default',
                                            'title' => 'View',
                                        ])
                                        :Html::a('<i class="far fa-eye"></i>', ['view', 'id' => $model->meet_id, 'customer' => $model->customer_id], [
                                            'class' => 'btn btn-default',
                                            'title' => 'View',
                                        ]) ;
                                },
                                'update' => function ($url, $model, $key) {

                                    return $model->status==0?
                                        Html::a('<i class="fas fa-pencil-alt"></i>', ['update', 'id' => $model->meet_id, 'customer' => $model->customer_id,'status'=>'edit'], [
                                            'class' => 'btn btn-default',
                                            'title' => 'Update',
                                        ])
                                        : Html::a('<i class="fas fa-pencil-alt"></i>', ['update', 'id' => $model->meet_id, 'customer' => $model->customer_id], [
                                        'class' => 'btn btn-default',
                                        'title' => 'Update',
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->meet_id, 'customer' => $model->customer_id],
                                        ['data-method' => 'post',
                                            'data-confirm' => 'Are you sure you want to delete this item?',
                                            'title' => 'Delete',
                                            'class' => 'btn btn-default',
                                            'data-pjax' => '0',
                                        ]);
                                }
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>


