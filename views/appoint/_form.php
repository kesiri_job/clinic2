<?php

use kartik\widgets\DateTimePicker;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Appointment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appointment-form">
    <div class="col-lg-12">
        <div class="portlet">
            <div class="portlet-heading ">
                <h2 class="portlet-title text-dark">
                    <?= empty($_GET['active']) ? "เพิ่มข้อมูล" : "แก้ไขข้อมูล" ?>
                </h2>
            </div>
            <div id="bg-primary" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <div class="col-md-9">
                            <?= $form->field($model, 'appoint_name')->textInput(['maxlength' => true])->label('หัวข้อการเข้าพบ') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'appoint_date')->widget(DateTimePicker::classname(), [
                                'options' => ['placeholder' => 'Enter date'],
                                'pluginOptions' => [
                                    'autoclose' => true
                                ]
                            ])->label('วันที่และเวลานัดพบ'); ?>
                        </div>
                    </div>
                    <?= $form->field($model, 'customer_id')
                        ->widget(Select2::classname(), [
                            'data' => \yii\helpers\ArrayHelper::map(\app\models\Customer::find()->all(), 'customer_id', 'customer_name'),
                            'options' => ['placeholder' => 'ชื่อผู้นัดพบ'],
                            'pluginOptions' => [
                                'allowClear' => false,
                            ],
                        ])->label('ชื่อผู้นัดพบ'); ?>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'type_pay')
                                ->widget(Select2::classname(), [
                                    'data' => ['แบ่งจ่าย' => 'แบ่งจ่าย', 'จ่ายครั้งเดียว' => 'จ่ายครั้งเดียว'],
                                    'options' => ['placeholder' => 'การจ่ายเงิน'],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                    ],
                                ]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'deposit')->textInput(['type'=>'number','maxlength' => true])?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'full_price')->textInput(['type'=>'number','maxlength' => true])?>
                        </div>
                    </div>
                    <?= $form->field($model, 'appoint_detail')->textarea(['maxlength' => true])->label('รายละเอียด') ?>
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'status')->dropDownList(['0' => 'ยังไม่เข้าพบ', '1' => 'เข้าพบแล้ว']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'call')->dropDownList(['0' => 'ยังไม่โทรตาม', '1' => 'โทรตามแล้ว'])->label('สถานะการโทร') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

