<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    NavBar::begin([
        'brandLabel' => 'CHER CHARM',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => '<i class="fas fa-home"></i> Home', 'url' => ['/site/index']],
            ['label' => '<i class="fas fa-pills"></i> Stock','visible' => !Yii::$app->user->isGuest, 'items' => [
                ['label' => 'Stock', 'url' => ['/stock/index'],'visible' => !Yii::$app->user->isGuest],
                ['label' => 'การใช้ยา', 'url' => ['/used/index'],'visible' => !Yii::$app->user->isGuest],
            ]],
            ['label' => '<i class="fas fa-users"></i> Customer', 'url' => ['/customer/index'],'visible' => !Yii::$app->user->isGuest],
            ['label' => '<i class="far fa-comments"></i> Appointment', 'url' => ['/appoint/index'],'visible' => !Yii::$app->user->isGuest],
            ['label' => '<i class="fas fa-user-edit"></i> Management','visible' => !Yii::$app->user->isGuest, 'items' => [
                ['label' => ' Approve User', 'url' => ['/users/approve'],'visible' => !Yii::$app->user->isGuest],
                ['label' => ' Add Doctor', 'url' => ['/doctor/index'],'visible' => !Yii::$app->user->isGuest],
            ]],
            ['label' => '<i class="fas fa-dollar-sign"></i> Account','visible' => !Yii::$app->user->isGuest, 'items' => [
                ['label' => ' Income', 'url' => ['/account/income'],'visible' => Yii::$app->user->id==1],
                ['label' => ' Income', 'url' => ['/account/income'],'visible' => Yii::$app->user->id==8],
                ['label' => ' Income', 'url' => ['/account/income'],'visible' => Yii::$app->user->id==9],
                ['label' => ' Income', 'url' => ['/account/income'],'visible' => Yii::$app->user->id==10],
                ['label' => ' Expend', 'url' => ['/account/expend'],'visible' => Yii::$app->user->id==1],
                ['label' => ' Expend', 'url' => ['/account/expend'],'visible' => Yii::$app->user->id==8],
                ['label' => ' Expend', 'url' => ['/account/expend'],'visible' => Yii::$app->user->id==9],
                ['label' => ' Expend', 'url' => ['/account/expend'],'visible' => Yii::$app->user->id==10],
            ]],
            ['label' => '<i class="fas fa-briefcase"></i> Work', 'url' => ['/work/index'],'visible' => !Yii::$app->user->isGuest],
                Yii::$app->user->isGuest ?
                ['label' => 'Sign in', 'url' => ['/user/security/login']] :
            ['label' => 'User(' . Yii::$app->user->identity->username . ')', 'items' => [

                ['label' => 'Profile', 'url' => ['/user/settings/profile']],
                ['label' => 'Account', 'url' => ['/user/settings/account']],
                ['label' => 'Logout', 'url' => ['/user/security/logout'], 'linkOptions' => ['data-method' => 'post']],
            ]],
            ['label' => 'Register', 'url' => ['/user/registration/register'], 'visible' => Yii::$app->user->isGuest],


        ],
        'encodeLabels' => false,
        'activateParents' => true,
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'หน้าหลัก',
                'url' => Yii::$app->homeUrl,
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <div class="row">
            <div class="col-md-12">
                <?php Yii::$app->formatter->locale = 'th_TH'; ?>
                <?= $content ?>
            </div>
        </div>


    </div>
</div>

<!--<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; One Geo Survey Co., Ltd. <? /*= date('Y') */ ?></p>
<p class="pull-right"><?php /*//Yii::powered() */ ?></p>
    </div>
</footer>-->
<?php foreach (Yii::$app->session->getAllFlashes() as $message):; ?>
    <?php
    echo \kartik\widgets\Growl::widget([
        'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
        'title' => (!empty($message['title'])) ? Html::encode($message['title']) : 'Title Not Set!',
        'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
        'body' => (!empty($message['message'])) ? Html::encode($message['message']) : 'Message Not Set!',
        'showSeparator' => true,
        'delay' => 1, //This delay is how long before the message shows
        'pluginOptions' => [
            'delay' => (!empty($message['duration'])) ? $message['duration'] : 2000, //This delay is how long the message shows for
            'placement' => [
                'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
            ]
        ]
    ]);
    ?>
<?php endforeach; ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
