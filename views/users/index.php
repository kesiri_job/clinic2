<?php
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Approve User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portlet">
    <div class="portlet-heading ">
        <h2 class="portlet-title text-dark">
            <?= Html::encode($this->title) ?>
        </h2>
        <div class="portlet-widgets">
        </div>
    </div>
    <div id="bg-primary" class="panel-collapse collapse in">
        <div class="portlet-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'tableOptions' => [
                    'class' => 'table-responsive',
                ],
                'layout' => '{items}{summary}{pager}',
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],
                    'username',
                    'email:email',

                    [
                        'attribute'=>'status',
                        'format'=>'html',
                        'filter'=>$searchModel->itemStatus,
                        'value'=>function($model){
                            return $model->statusName=='Active' ?'<span class="label label-success">'.$model->statusName.'</span>' :'<span class="label label-warning">'. $model->statusName.'</span>' ;
                        }
                    ],
                    ['class' => 'kartik\grid\ActionColumn',
                        'options' => ['style' => 'width:140px;'],
                        'buttonOptions'=>['class'=>'btn btn-default'],
                        'template'=>'<div class="btn-group btn-group-sm text-center" role="group"> {view} {update} </div>',],
                ],
            ]); ?>
        </div>
    </div>
</div>

