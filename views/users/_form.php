<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Userabt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="userabt-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="portlet">
        <div class="portlet-heading ">
            <h2 class="portlet-title text-dark">
              ปรับปรุงข้อมูล
            </h2>
        </div>
        <div id="bg-primary" class="panel-collapse collapse in">
            <div class="portlet-body">
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(['1'=>'Active','0'=>'Not Active']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
