<?php

use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">
    <div class="col-lg-12">
        <div class="portlet">
            <div class="portlet-heading ">
                <h2 class="portlet-title text-dark">
                    <?= empty($_GET['active']) ? "เพิ่มข้อมูล" : "แก้ไขข้อมูล" ?>
                </h2>
            </div>
            <div id="bg-primary" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'file')->widget(Upload::classname(), ['url' => ['avatar-upload']])->label('รูปภาพ') ?>
                        </div>
                        <div class="col-md-9">
                            <?= $form->field($model, 'id_card')->textInput(['maxlength' => true])->label('รหัสประจำตัวประชาชน') ?>

                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true])->label('ชื่อ-นามสกุล') ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true])->label('เบอร์โทร') ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <?= $form->field($model, 'nicname')->textInput(['maxlength' => true])->label('ชื่อเล่น') ?>
                                </div>
                                <div class="col-md-4">
                                    <?= $form->field( $model, 'gender' )
                                        ->widget( \kartik\select2\Select2::classname(), [
                                            'data' => ['หญิง' => 'หญิง', 'ชาย' => 'ชาย'],
                                            'options' => ['placeholder' => ''],
                                            'pluginOptions' => [
                                                'allowClear' => false,
                                            ],
                                            //'theme'=> \kartik\select2\Select2::THEME_DEFAULT,
                                        ] )->label('เพศ'); ?>
                                    <?php // $form->field($model, 'gender')->dropDownList(['หญิง' => 'หญิง', 'ชาย' => 'ชาย'], ['maxlength' => true])->label('เพศ') ?>
                                </div>
                                <div class="col-md-4">
                                    <?= $form->field($model, 'age')->textInput(['type' => 'number'])->label('อายุ') ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <?=
                                    $form->field($model, 'birth_dath')->widget(\kartik\widgets\DatePicker::classname(), [
                                        'options' => ['placeholder' => 'Enter birth date ...'],
                                        'pluginOptions' => [
                                            'autoclose'=>true,
                                             'format' => 'yyyy-mm-dd'
                                        ]
                                    ]); ?>
                                </div>
                                <div class="col-md-4">
                                    <?= $form->field($model, 'height')->textInput(['type' => 'number'])->label('ส่วนสูง') ?>
                                </div>
                                <div class="col-md-4">
                                    <?= $form->field($model, 'weight')->textInput(['type' => 'number'])->label('น้ำหนัก') ?>
                                </div>
                            </div>
                            <?= $form->field($model, 'career')->textInput(['maxlength' => true])->label('อาชีพ') ?>
                            <?= $form->field($model, 'disease')->textInput(['maxlength' => true])->label('โรคประจำตัว') ?>
                            <?= $form->field($model, 'history_allergy')->textarea(['maxlength' => true])->label('ประวัติการแพ้ยา') ?>
                            <div class="form-group">
                                <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>