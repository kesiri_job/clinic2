<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/7/2018
 * Time: 1:28 PM
 */
use miloschuman\highcharts\Highcharts;
$this->title = 'Graph';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="portlet">
    <div class="portlet-heading ">
        <h2 class="portlet-title text-dark">
            Graph
        </h2>
    </div>
    <div id="bg-primary" class="panel-collapse collapse in">
        <div class="portlet-body">
            <?php
            echo Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],

                'options' => [
                    'title' => [
                        // 'text' => 'กราฟแสดงรายการสูงสุด 5 อันดับแรก',
                    ],
                    'xAxis' => [
                        'classname'=>'highcharts-color-0',
                        'categories' => ['รายการ'],
                    ],
                    'credits' => ['enabled' => false],
//                            'colors'=> ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572',
//                                '#FF9655', '#FFF263', '#6AF9C4'],
                    'labels' => [
                        'items' => [
                            [
                                'style' => [
                                    'left' => '50px',
                                    'top' => '18px',
                                    'color' => new \yii\web\JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                ],
                            ],
                        ],
                    ],
                    'series' => [

                        [
                            'type' => 'column',
                            'name' => empty($main_items[0]['name']) ? null : $main_items[0]['name'],
                            'data' => [ empty($main_items[0]['count']) ? null : (int)$main_items[0]['count'],],
                        ],
                        [
                            'type' => 'column',
                            'name' => empty($main_items[1]['name']) ? null : $main_items[1]['name'],
                            'data' => [ empty($main_items[1]['count']) ? null : (int)$main_items[1]['count'],],
                        ],
                        [
                            'type' => 'column',
                            'name' => empty($main_items[2]['name']) ? null : $main_items[2]['name'],
                            'data' => [ empty($main_items[2]['count']) ? null : (int)$main_items[2]['count'],],
                        ],
                        [
                            'type' => 'column',
                            'name' => empty($main_items[3]['name']) ? null : $main_items[3]['name'],
                            'data' => [ empty($main_items[3]['count']) ? null : (int)$main_items[3]['count'],],
                        ],
                        [
                            'type' => 'column',
                            'name' => empty($main_items[4]['name']) ? null : $main_items[4]['name'],
                            'data' => [ empty($main_items[4]['count']) ? null : (int)$main_items[4]['count'],],
                        ],



                    ],
                ]
            ]);
            ?>
        </div>
    </div>
</div>

