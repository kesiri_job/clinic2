<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DoctorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Doctors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doctor-index">


    <?php
    $this->registerJs('
        function init_click_handlers(){
            $("#activity-create-link").click(function(e) {
                    $.get(
                        "create",
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                             $(".modal-title").html("เพิ่มข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });
            $(".activity-view-link").click(function(e) {
                    var fID = $(this).closest("tr").data("key");
                    $.get(
                        "view",
                        {
                            id: fID
                        },
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                            $(".modal-title").html("เปิดดูข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });
            $(".activity-update-link").click(function(e) {
                    var fID = $(this).closest("tr").data("key");
                    $.get(
                        "update",
                        {
                            id: fID
                        },
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                            $(".modal-title").html("ปรับปรุงข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });     
        }
        init_click_handlers(); //first run
        $("#customer_pjax_id").on("pjax:success", function() {
          init_click_handlers(); //reactivate links in grid after pjax update
        });');?>
    <div class="col-lg-12">
        <div class="portlet">
            <div class="portlet-heading ">
                <h2 class="portlet-title text-dark">
                    <?= Html::encode($this->title) ?>
                </h2>
                <div class="portlet-widgets">
                    <p>
                        <?= Html::button('<i class="zmdi zmdi-collection-plus"></i> Create Doctor', [ 'class' => 'btn btn-success','id'=>'activity-create-link']); ?>
                        <?php // Html::a('<i class="zmdi zmdi-collection-plus"></i> Create Doctor', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                </div>
            </div>
            <div id="bg-primary" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <?= $this->render('_search', ['model' => $searchModel]); ?>
                    <br/>
                    <?php Pjax::begin(['id'=>'pjax_1']); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'responsiveWrap' => false,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'kartik\grid\SerialColumn', 'options' => ['style' => 'width:60px;'],],
                            'doctor_name',
                            ['class' => 'kartik\grid\ActionColumn',
                                'template'=>'<div class="btn-group btn-group-sm text-center" role="group"> {update} | {delete} </div>',
                                'options' => ['style' => 'width:130px;'],
                            'buttons'=>[
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', ['update', 'id' => $model->doctor_id], [
                                        'class' => 'activity-update-link',
                                        'title' => 'Update',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#activity-modal',
                                        'data-id' => $key,
                                        'data-pjax' => '0',
                                    ]);
                                },
                                ],
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php \yii\bootstrap\Modal::begin([
    'id' => 'activity-modal',
    'header' => '<h4 class="modal-title"></h4>',
    'size'=>'modal-lg',
    'clientOptions' => ['backdrop' => false],
    'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">ปิด</a>',
]);
\yii\bootstrap\Modal::end();
?>