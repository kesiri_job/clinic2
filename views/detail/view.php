<?php

use app\controllers\GetpublicController;
use app\models\Profile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AbtDetail */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลการเข้าพบ', 'url' => ['index', 'id' => $model->cw_running]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="abt-detail-view">

    <h1><?= Html::encode( $this->title ) ?></h1>

    <p>
        <?= Html::a( 'แกไขข้อมูล', ['update', 'id' => $model->dt_running], ['class' => 'btn btn-primary'] ) ?>
        <?= Html::a( 'ลบข้อมูล', ['delete', 'id' => $model->dt_running], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ] ) ?>
    </p>
<br/>
    <?php
    foreach ($modelMeet as $item){
        $array[] =  implode(',',ArrayHelper::map(Profile::find()->where(['detail_user_meet.user_id'=>$item])->innerJoin('detail_user_meet', 'detail_user_meet.user_id = profile.user_id')->all(),'user_id','name'));
    }

    ?>

    <?= DetailView::widget( [
        'model' => $model,
        'attributes' => [
            [
                'label' => 'วันที่เข้าพบ',
                'value' => GetpublicController::getDateThai($model->visitdate),
            ],
            'title',
            [
                'format'=>'html',
                'label' => 'รายละเอียด',
                'value' => $model->dt_detail,
            ],
            [
                'format'=>'html',
                'label' => 'ผู้เข้าพบ',
                'value' => empty($array) ? null :implode(',',$array),

            ],
            'creator',
            [
                'label' => 'วันที่เพิ่มข้อมูล',
                'value' => GetpublicController::getDateThai($model->cr_date),
            ],
            'updater',
            [
                'label' => 'วันที่แก้ไขข้อมูล',
                'value' => GetpublicController::getDateThai($model->upd_date),
            ],
        ],
    ] ) ?>



    <br/>
    <div class="col-md-12">
        <strong>เอกสาร</strong><br>
        <?php

        foreach ($model->getPictureAttach() as $item) {
            if (!$item['isImage']) {
                ?>

                - <a href="<?= $item['src'] ?>"><?= $item['options']['title'] . '<br>' ?></a>


                <?php
            }
        } ?>
    </div>

</div>
<br>
<br>
<br>
<div class="col-sm-12">
    <?php if (count( $model->getPictureAttach() ) != 0) { ?>
        <strong>รูปภาพ</strong>
        <?= dosamigos\gallery\Gallery::widget( ['items' => $model->getPictureAttach()] ); ?>
    <?php } ?>
</div>
