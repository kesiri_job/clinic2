<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ExpendSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Expends';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$this->registerJs('
        function init_click_handlers(){
            $("#activity-create-link").click(function(e) {
                    $.get(
                        "expend-create",
                        function (data)
                        {                        
                            $("#activity-modal").find(".modal-body").html(data);    
                            $(".modal-body").html(data);
                              tinymce.remove();
                              tinymce.init({selector: "textarea"});
                             $(".modal-title").html("เพิ่มข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });
            $(".activity-view-link").click(function(e) {
                    var fID = $(this).closest("tr").data("key");
                    $.get(
                        "expend-view",
                        {
                            id: fID
                        },
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                            $(".modal-title").html("เปิดดูข้อมูล");
                            $("#activity-modal").modal("show");
                            
                        }
                    );
                });
            $(".activity-update-link").click(function(e) {
                    var fID = $(this).closest("tr").data("key");
                    $.get(
                        "expend-update",
                        {
                            id: fID
                        },
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                              tinymce.remove();
                              tinymce.init({selector: "textarea"});
                            $(".modal-title").html("ปรับปรุงข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });     
        }
        init_click_handlers(); //first run
        $("#customer_pjax_id").on("pjax:success", function() {
          init_click_handlers(); //reactivate links in grid after pjax update
         
        });'); ?>
<div class="expend-index">
    <div class="col-lg-12">
        <div class="portlet">
            <div class="portlet-heading ">
                <h2 class="portlet-title text-dark">
                    <?= Html::encode($this->title) ?>
                </h2>
                <div class="portlet-widgets">
                    <p>
                        <?php // Html::button('<i class="zmdi zmdi-collection-plus"></i> เพิ่มการนัดพบ', [ 'class' => 'btn btn-success','id'=>'activity-create-link']); ?>
                        <?= Html::button('<i class="zmdi zmdi-collection-plus"></i> Create Expend', ['class' => 'btn btn-success', 'id' => 'activity-create-link']); ?>

                    </p>
                </div>
            </div>
            <div id="bg-primary" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-10">
                            <?php echo $this->render('expend__search', ['model' => $searchModel]); ?>
                        </div>
                        <div class="col-md-2">
                            <div align="right">
                                <br/><br/><br/>
                                <?php
                                $gridColumns = [
                                    ['class' => 'kartik\grid\SerialColumn'],
                                    'expend_date',
                                    'expend_name',
                                    'expend_number',
                                    [
                                        'attribute' => 'detail',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return Html::a($model->detail, '#', []);
                                        },
                                        'format' => 'html'
                                    ],
                                    [
                                        'attribute' => 'created_by',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return   $model->created_by?\app\models\Userabt::findOne($model->created_by)->username:null;
                                        },
                                        'format' => 'html'
                                    ],
                                    [
                                        'attribute' => 'updated_by',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return   $model->updated_by?\app\models\Userabt::findOne($model->updated_by)->username:null;
                                        },
                                        'format' => 'html'
                                    ],


                                    /*  ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
                                          return '#';
                                      }]*/
                                ];

                                echo ExportMenu::widget([
                                    'dataProvider' => $dataProvider,
                                    'columns' => $gridColumns,
                                    'fontAwesome' => true,
                                    'target' => '_blank',
                                    'exportConfig' => [
                                        ExportMenu::FORMAT_TEXT => false,
                                        ExportMenu::FORMAT_PDF => false,
                                        ExportMenu::FORMAT_CSV => false,
                                    ],
                                ]);


                                ?>
                            </div>
                        </div>
                    </div>

                    <?php Pjax::begin(['id' => 'pjax_69']); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                       // 'filterModel' => $searchModel,
                        'responsiveWrap' => false,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'kartik\grid\SerialColumn'],
                            'expend_name',
                            'expend_number',
                            [
                                'attribute' => 'expend_date',
                                'value' => function ($data) {
                                    return \app\controllers\GetpublicController::getDateThaiTime($data->expend_date);
                                }
                            ],
                            ['class' => 'kartik\grid\ActionColumn',
                                'options' => ['style' => 'width:140px;'],
                                'buttonOptions' => ['class' => 'btn btn-default'],
                                'template' => '<div class="btn-group btn-group-sm text-center" role="group"> {view} {update} {delete} </div>',
                                'buttons' => [
                                    'view' => function ($url, $model, $key) {
                                        return Html::a('<i class="far fa-eye"></i>', ['expend-view', 'id' => $model->id], [
                                            'class' => 'activity-view-link btn btn-default',
                                            'title' => 'View',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#activity-modal',
                                            'data-id' => $key,
                                            'data-pjax' => '0',
                                        ]);
                                    },
                                    'update' => function ($url, $model, $key) {
                                        return Html::a('<i class="fas fa-pencil-alt"></i>', ['expend-update', 'id' => $model->id], [
                                            'class' => 'btn btn-default activity-update-link',
                                            'title' => 'Update',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#activity-modal',
                                            'data-id' => $key,
                                            'data-pjax' => '0',
                                        ]);
                                    },
                                    'delete' => function ($url, $model, $key) {
                                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', ['expend-delete', 'id' => $model->id],
                                            ['data-method' => 'post',
                                                'data-confirm' => 'Are you sure you want to delete this item?',
                                                'title' => 'Delete',
                                                'class' => 'btn btn-default',
                                                'data-pjax' => '0',
                                            ]);
                                    }
                                ],
                                ],
                        ],
                    ]); ?>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php \yii\bootstrap\Modal::begin([
    'id' => 'activity-modal',
    'header' => '<h4 class="modal-title"></h4>',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => false],
    'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">ปิด</a>',
]);
\yii\bootstrap\Modal::end();
?>
